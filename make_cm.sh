#!/bin/bash

set -o errexit
set -o nounset
set -o xtrace

sprint=$1

source .venv/bin/activate
./cm.py > "cm$sprint.md"
pandoc "cm$sprint.md" -f markdown -t docx -s -o "cm-$sprint.docx"
