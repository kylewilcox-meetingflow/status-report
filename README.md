# status-report

I have a hard time remembering exactly what I've done since the last time I gave
a status report. Thankfully we have benevolent computers tracking our every moves.

This is a small tool to output what you've been working on, by pulling data from
GitLab and Jira.

Currently only works for GitLab. Jira is WIP because I can't find the right API to
hit. It looks like the frontend is powered by a JQL query which I might have to replicate.

## Setup

```sh
python3 -m venv .venv
source .venv/bin/activate.fish  # Or .venv/bin/activate if you're usisng sh/bash
pip install -r requirements.txt
# Get a personal access token from here:
#  https://gitlab.com/-/profile/personal_access_tokens
echo GITLAB_API_TOKEN=SECRET > .env
```

TODO: Dockerize it, and write a shell script to pull and run it and mount in `.env`.

## Run

```sh
./status.py
```
